require('dotenv').config()

var randomstring = require('randomstring'),
    cookieParser = require('cookie-parser'),
    Promise = require('bluebird'),
    fetch = require('node-fetch'),
    MCrypt = require('mcrypt').MCrypt

var express = require('express'),
    app = express();

var rj256 = new MCrypt('rijndael-256', 'cbc');
let key = process.env.SHARED_KEY; //shared key from 3rd party

let config = {
  "shared_secret": "",
  "sso_url": "",
  "task": "/app/sso/custom/",
  //"task" => "/podium/Default.aspx?t=52841",
  "salt_key": "wh_s",
  "return_key": "rt_url",
  "vendor_key": "vendorkey",
  "vendor": "wsapi",
  "timeout": 60,
  "token_key": "wh_sso_login",
  "return_url": "",
  "dev_sid": "",
  "sign_out_return_url":""
};

app.use(cookieParser())

function decrypt(wh_data, iv) {
    rj256.open(key, iv)
    return rj256.decrypt(new Buffer(wh_data, 'base64'))
}

function encrypt(url, iv) {
    rj256.open(key, iv)
    return rj256.encrypt(url).toString('base64')
}

function get_url(url) {
  var iv = randomstring.generate(32)
  var encrypted_url = encrypt(url, iv)
  return redirectedUrl = process.env.API_URL+'/app/sso/custom'+
                        '?rt_url='+encodeURIComponent(encrypted_url)+
                        '&wh_s='+encodeURIComponent(iv)
}

function sign_out(rt_url) {
	return process.env.API_URL+"/app/sso/logout?rt_url="+rt_url;
}

app.get('/', (req, res) => {
  var current_url = 'http://'+req.headers.host+'/validate';
  if(req.query.wh_sso_login && req.query.wh_s) {
    res.send(req.query)
  }else {
    var login_url = get_url(current_url)
    res.redirect(login_url)
  }
})

app.get('/validate', (req, res) => {
  console.log(req.cookies)
  var obj = req.query
  var decryptedData = decrypt(obj.wh_sso_login, obj.wh_s).toString()
  var user = decryptedData.split("||")
  /*
  ---
    user info is now accessible, problem is that we don't have a token to use with all of the API endpoints
  ---
  */
  res.send(user)
})

app.get('/logout', (req, res) => {
  console.log(req.headers)
  res.redirect(sign_out('http://'+req.headers.host))
})

app.listen(4444)
